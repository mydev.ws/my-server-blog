# Security

## How to manage secrets

### First idea is to use gpg2 and pass

```bash
sudo apt-get install gnupg2 pass -y
gpg2 --full-generate-key

# Select the default key type (RSA), the default key size (3072), a 0 expiration (which means it never expires), and answer Y that everything is correct. 
# You will then add your name to the key, an email address, a comment (whatever you like), and finally, give the key a passphrase

pass init EMAIL

pass insert {password_name}
pass {password_name}
```

https://www.techrepublic.com/article/how-to-safely-store-passwords-linux-server/

But there is no option to pass the master secrete as file or as value to use it in scripts.
I want my server to be restartable without manual action

### What about store passwords in a file in a root space?

Finally if I want my server to restart without manual input, then I need to store at least one master password.
If the atacker has access to root non of the local tools like pass will prevent them from accessing the secrets.

But then I have to think about rootless docker 

* https://thenewstack.io/how-to-run-docker-in-rootless-mode/
* https://rootlesscontaine.rs/

I will pospone to address it, because today I would not be able to distiguish if a problem is caused by luck of 
knowlege about docker or by the rootless setup.

### What about docker secrets?

After first reading sounds good:

* encrypted on rest
* encrypted in transfer
* api to manage
* built-in support in docker compose

But I have to switch to docker swarm! Hmmm it adds complexity which I do not need in my single hardware setup.
Docker swarm is some kind of alternative to kubernetes which is de facto standard. 

Should I then go for kubernetes?
No, I try to stay as simple as possible. But I have to learn some new commands from docker swarm.

```bash

# instead of docker-compose up -d
docker stack deploy --compose-file=docker-compose.yml <name-of-the-stack>

# instead of docker-compose down
docker stack rm <name-of-the-stack>

```
#### Secrets in docker swarm

First create secrete

```bash
# create secret
docker secret create my-secret_v1 <my secrete value>

#todo learn how to exclude <my secrete value> from shell history. For now just using portainer.
```

then use it in docker-compose

```yml
version: "3.9"
services:
  mydev:
    image: <image-name>
    stdin_open: true # docker run -i
    tty: true # docker run -t
    entrypoint: /bin/sh
    volumes:
      # Data persistency
    secrets:
      - my-secret

secrets:
  my-secret: 
    external: true
    name: my-secret_v1

```

then rotate it when required


```bash
# rotate a secret
docker service update \
    --secret-rm mysql_password mysql
docker service update \
    --secret-add source=mysql_password,target=old_mysql_password \
    --secret-add source=mysql_password_v2,target=mysql_password \
    mysql
```
https://docs.docker.com/engine/swarm/secrets/
