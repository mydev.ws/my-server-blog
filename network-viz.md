# Netwok viz

Start simple!

## Problem statement

I want to have a docker container that, provides a historical view which devices are online in my network.

## Solution idea

* ping all devices in my network
* push results in a InfluxDB
* create some kind of visualisiation which can be use on my smartphone

### technology

Shell or python? I'm a beginner in both:

* Python: 
    * pros
        * more natural for me coming from Java, Typescript 
        * is installed on many linux images by default
        * has a lot libs for networking (e.g. scapy)
        * suppors venv (virtual environment)
    * cons
        * managing libs is less convinient for me than in npm
        * setup IDE will take time

* Shell
    * pros
        * has a lib fping solving the first part of my challenge
        * is installed on any linux image by default
    * cons
        * working with strings instead of objects is a less convinient for me
        * no idea if I can push data to InfluxDB

### Lets try with python

I spent about 5 hours to figure out how the venv works together with VS Code. 

As a result I use a shell script hooking into **cd** command and activate/deactivate venv. 
https://gitlab.com/mydev.ws/my-server/-/blob/main/scripts/virtualenv-auto-activate.sh

The scritp assumes that venv is geenrated into ./.venv directory. 

Execute python files on save still not working. I will solve it later.

### Networking and challenges 

All my programming experience is about building applications. 
There are a lot of new things to learn about network low level protocols!

Ping is most simple way to check if device is online in the network, 
but a device can use a "stealth" mode, not replying on ping messages. -> For the first mvp, I accept this limitation.

#### Reverse DNS problem with Pi Hole

None of the approaches: ping, scapy, nslookup, fping can get a host name from the ip address!

What is wrong?

The only host returning the right host name is Pi Hole! Lets try to bypass Pi Hole as DNS

```bash
# set my router manually as DNS server
nslookup 192.168.178.106 192.168.178.1
# result is the right host name behind the given ip
```


After a research on Pi Hole sites I came to a conclusion that Pi Hole will not fix it.

What if I change my runtime (inside docker) to use my router as DNS server? -> Its time to put my code into a container

#### Efficient development with docker

The approach I know from my professional work is:

* create docker files
* setup ci/cd to build an image, scan it and push it to a registry
* deploy the docke image from registry to a server

For the first time I need a much more interactive way to develop in docker! 

What if I mount my code as volume to a python docker container? 

docker-compose.yaml

```yaml
version: "3.9"
services:
  mydev:
    image: python:latest
    stdin_open: true # docker run -i
    tty: true # docker run -t
    container_name: mydev
    entrypoint: /bin/sh
    volumes:
      # Data persistency
      # sudo mkdir -p /srv/docker/influxdb/data
      - ~/my-server/network-viz:/srv/network-viz
```
and bash script to start interactive ssh session inside the container

```bash
#!/bin/bash
docker exec -ti mydev /bin/bash
```

It works well! On restart of the container the environment is clean again and i can experiment.

Changes I want to be permanent a can add to another docker file, which would be the production one.


